php-horde-text-filter (2.3.6-3) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
    - protect the -, this is not a range (Closes: #935816)
  * salsa-ci.yml: Allow autopkgtest failure
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 21:34:21 +0200

php-horde-text-filter (2.3.6-2) unstable; urgency=medium

  * Standards-Version: 4.4.0, no change
  * Source-only upload

 -- Mathieu Parent <sathieu@debian.org>  Mon, 15 Jul 2019 09:47:20 +0200

php-horde-text-filter (2.3.6-1) unstable; urgency=medium

  * New upstream version 2.3.6
    - regexps: protect the -, this is not a range (Closes: #931255)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 14 Jul 2019 08:19:50 +0200

php-horde-text-filter (2.3.5-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 09:22:54 +0200

php-horde-text-filter (2.3.5-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 14:22:07 +0200

php-horde-text-filter (2.3.5-1) unstable; urgency=high

  * New upstream version 2.3.5
    + SECURITY: Fix XSS with data:html links and form actions.
      Closes: #837150 and urgency set to high

 -- Mathieu Parent <sathieu@debian.org>  Fri, 09 Sep 2016 13:54:11 +0200

php-horde-text-filter (2.3.4-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 06:53:26 +0200

php-horde-text-filter (2.3.4-1) unstable; urgency=medium

  * New upstream version 2.3.4

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 11:25:15 +0100

php-horde-text-filter (2.3.3-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 11:45:07 +0100

php-horde-text-filter (2.3.3-1) unstable; urgency=medium

  * New upstream version 2.3.3

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 22:18:32 +0100

php-horde-text-filter (2.3.2-2) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 21:35:23 +0200

php-horde-text-filter (2.3.2-1) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf
  * New upstream version 2.3.2

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 23:59:57 +0200

php-horde-text-filter (2.3.1-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.3.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 09:40:15 +0200

php-horde-text-filter (2.2.1-5) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 13:27:14 +0200

php-horde-text-filter (2.2.1-4) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 12:38:14 +0200

php-horde-text-filter (2.2.1-3) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:35:29 +0200

php-horde-text-filter (2.2.1-2) unstable; urgency=medium

  * Downgrade Horde_Text_Filter_Jsmin recommends to suggests
    - d/pkg-php-tools-overrides: Horde_Text_Filter_Jsmin overrided to none
      (Closes: #751491)
    - d/control: add php-horde-text-filter-jsmin in suggests

 -- Mathieu Parent <sathieu@debian.org>  Sun, 15 Jun 2014 21:29:17 +0200

php-horde-text-filter (2.2.1-1) unstable; urgency=medium

  * New upstream version 2.2.1

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 May 2014 13:09:05 +0200

php-horde-text-filter (2.2.0-1) unstable; urgency=low

  * New upstream version 2.2.0
    - JsMin text filter is now optional (Closes: #705317, #706151, #730117)

 -- Mathieu Parent <sathieu@debian.org>  Sat, 30 Nov 2013 20:47:16 +0100

php-horde-text-filter (2.1.5+debian0-1) unstable; urgency=low

  * New upstream version 2.1.5+debian0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 29 Oct 2013 19:19:12 +0100

php-horde-text-filter (2.1.4+debian0-1) unstable; urgency=low

  * New upstream version 2.1.4+debian0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 22 Oct 2013 18:03:10 +0200

php-horde-text-filter (2.1.3+debian0-1) unstable; urgency=low

  * New upstream version 2.1.3, repacked

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 19:27:51 +0200

php-horde-text-filter (2.1.2+debian0-2) unstable; urgency=low

  * Use pristine-tar (with filtering)
  * Silence check_jsmin

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:25:21 +0200

php-horde-text-filter (2.1.2+debian0-1) unstable; urgency=low

  * New upstream version 2.1.2
  * Update changelog of 2.0.3+debian0-2 to avoid lintian warning

 -- Mathieu Parent <sathieu@debian.org>  Thu, 23 May 2013 22:18:47 +0200

php-horde-text-filter (2.1.1+debian0-1) unstable; urgency=low

  * New upstream version 2.1.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 14 May 2013 21:06:26 +0200

php-horde-text-filter (2.1.0+debian0-1) unstable; urgency=low

  * New upstream version 2.1.0

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 18:06:40 +0200

php-horde-text-filter (2.0.4+debian0-1) unstable; urgency=low

  * New upstream version 2.0.4+debian0

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 22:36:28 +0100

php-horde-text-filter (2.0.3+debian0-3) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 21:20:53 +0100

php-horde-text-filter (2.0.3+debian0-2) unstable; urgency=low

  * Remove "non-free files in upstream tarball ("The Software shall be used
    for Good, ...")" (Closes: #692629)
    - Remove from upstream release
    - Fails if it still exists
    - Remove from package.xml after configure step (this requires
      pkg-php-tools >= 1.1)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 06 Jan 2013 19:22:18 +0100

php-horde-text-filter (2.0.3-1) unstable; urgency=low

  * New upstream version 2.0.3
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Homepage
  * Added Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 22:03:30 +0100

php-horde-text-filter (1.1.2-1) unstable; urgency=low

  * Horde_Text_Filter package.
  * Initial packaging (Closes: #635815)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Tue, 10 Jan 2012 22:35:21 +0100
